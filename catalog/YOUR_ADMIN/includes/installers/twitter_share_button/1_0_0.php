<?php
$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Twitter Share Button Configuration' ORDER BY configuration_group_id ASC;");
if ($configuration->RecordCount() > 0) {
  while (!$configuration->EOF) {
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $configuration->MoveNext();
  }
}

$db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = '';");


$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Twitter Share Button Configuration', 'Set Twitter Share Button Configuration Options', '1', '1');");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
            (NULL, 'Version', 'TS_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NULL, 'trim('),
            (NULL, 'Status', 'TS_STATUS', 'false', 'Enable Twitter Share Button?', " . $configuration_group_id . ", 1, NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
            (NULL, 'Share URL', 'TS_SHARE_URL', '', 'Leave blank for the button to share the page URL that the button is on (you must include http:// or https://).', " . $configuration_group_id . ", 2, NOW(), NULL, NULL),
            (NULL, 'Tweet Text', 'TS_TWEET_TEXT', '', 'Leave blank for the button to use the title of the page that it is on.', " . $configuration_group_id . ", 3, NOW(), NULL, NULL),
            (NULL, 'Show Count', 'TS_BUTTON_LAYOUT', 'true', 'Show the count next to the button.', " . $configuration_group_id . ", 4, NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),'),
            (NULL, 'Twitter Account', 'TS_TWITTER_ACCOUNT', '', 'Insert your twitter account username.', " . $configuration_group_id . ", 5, NOW(), NULL, NULL),
            (NULL, 'Large Button', 'TS_BUTTON_SIZE', 'false', 'Do you want a large button?', " . $configuration_group_id . ", 6, NOW(), NULL, 'zen_cfg_select_option(array(\"true\", \"false\"),');");

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // add configuration menu
  if (function_exists('zen_page_key_exists') && function_exists('zen_register_admin_page') && !zen_page_key_exists('configTS')) {
    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'TS_VERSION' LIMIT 1;");
    $configuration_group_id = $configuration->fields['configuration_group_id'];
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configTS',
                              'BOX_CONFIGURATION_TWITTER_SHARE_BTN', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Twitter Share Button Configuration Menu.', 'success');
    }
  }
}

$messageStack->add('Installed Twitter Share Button v1.0.0', 'success');