<?php
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }   
   
  // add upgrade script
  if(defined('TS_VERSION')){ $twitter_version =  TS_VERSION;}else{$twitter_version = "new"; }
  
  $current_version = "1.0.1";
  $zencart_com_plugin_id = 1783; // from zencart.com plugins - Leave Zero not to check

  if($google_plus_one_button_version != 'new') {
    $config = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key= 'TS_VERSION'");
    $configuration_group_id = $config->fields['configuration_group_id'];
}

  $stoperr = 0;
  while ($twitter_version != $current_version && $stoperr != 1) {
    switch($twitter_version) {
      case "new":
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/twitter_share_button/1_0_0.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/twitter_share_button/1_0_0.php');
          $twitter_version = "1.0.0";
          break;          
        } else {
		      $messageStack->add('Cannot install Twitter Share Button, please make sure you have uploaded all files in the package.', 'error');
		      break 2;
        }
      case "1.0.0":
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/twitter_share_button/1_1_0.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/twitter_share_button/1_1_0.php');
          $twitter_version = "1.1.0";
          break;          
        } else {
		      $messageStack->add('Cannot upgrade Twitter Share Button, please make sure you have uploaded all files in the package.', 'error');
		      break 2;
      	}
      default:
        $twitter_version = $current_version;
        // break all the loops
      	break 2;      
    }
  }    

  // Version Checking
if ($zencart_com_plugin_id != 0) {
    if ($_GET['gID'] == $configuration_group_id) {
        $new_version_details = plugin_version_check_for_updates($zencart_com_plugin_id, $current_version);
        if ($new_version_details != FALSE) {
            $messageStack->add("Version " . $new_version_details['latest_plugin_version'] . " of " . $new_version_details['title'] . ' is available at <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>', 'caution');
        }
    }
}

if (!function_exists('plugin_version_check_for_updates')) {

    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0) {
            return FALSE;
        }
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        // compare versions
        if (version_compare($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0) {
            $new_version_available = TRUE;
        }
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions'])) {
            $new_version_available = FALSE;
        }
        if ($version_string_to_check == true) {
            return $data[$lookup_index];
        } else {
            return FALSE;
        }
    }

}

?>